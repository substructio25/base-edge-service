package com.difinite.zuul.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/admin")
    public String getAdminMenu(){
        return "You are accessing admin menu";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping("/user")
    public String getUserMenu(){
        return "You are accessing user menu";
    }

    @RequestMapping("/")
    public String getNoRolesMenu(){
        return "You are accesing no roles menu";
    }
}
