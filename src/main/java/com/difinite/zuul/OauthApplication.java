package com.difinite.zuul;

import com.difinite.zuul.fallback.Fallback;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableZuulProxy
public class OauthApplication {
    public static void main(String[] args) {
            SpringApplication.run(OauthApplication.class, args);
    }
    
    @Bean
    public RestTemplate restTemplate() {
    	return new RestTemplate();
    }

    @Bean
    public FallbackProvider fallbackProvider(){
      return new Fallback();
    }
}

