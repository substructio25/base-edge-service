package com.difinite.zuul.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(name="dynamic_route", schema = "api")
public class DynamicRoute implements Serializable {
    @Id
    @Column(name = "id")
    public String requestURIUniqueKey;
    @Column(name = "request_uri")
    private String requestURI;
    @Column(name = "target_url_host")
    private String targetURLHost;
    @Column(name="target_url_port")
    private int targetURLPort;
    @Column(name="target_uri_path")
    private String targetURIPath;
    @Column(name="description")
    private String description;
    @Column (name="number_of_instance")
    private int numberOfInstance;
}
